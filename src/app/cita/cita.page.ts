import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../services/service.service';
import { LoadingController, ToastController, NavController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-cita',
  templateUrl: './cita.page.html',
  styleUrls: ['./cita.page.scss'],
})
export class CitaPage implements OnInit {
  dos: string;
  tres: string;
  data: any;
  show = false;
  show2 = false;
  show3 = false;
  peluqueros: any;
  dias: any;
  horarios: any;
  precio: any;
  id: any;
  form: FormGroup;
  total: number;
  user = localStorage.getItem('id');

  constructor(private service: ServiceService,
              public loading: LoadingController,
              public formB: FormBuilder,
              public toast: ToastController,
              public nav: NavController
  ) { }

  ngOnInit() {
    this.form = this.formB.group({
      primero: ['', Validators.required],
      segundo: ['', Validators.required],
      tercero: ['', Validators.required]
    });
  }

  ionViewWillEnter() {
    this.getPeluqueros();
    this.sumaTotal();
  }

  selectOptionValue2(event) {
    if (event.detail.value) {
      this.getDiaspeluquero(event.detail.value);
      this.id = event.detail.value;
      this.dos = '';
      this.tres = '';
      this.show2 = true;
    }
  }

  selectOptionValue3(event) {
    if (event.detail.value) {
      this.getHorarios(event.detail.value, this.id);
      this.tres = '';
      this.show3 = true;
    }
  }

  async getPeluqueros() {
    const loading = await this.loading.create({
      message: 'Loading...',
    });
    await loading.present().then(() => {
      this.service.getPeluqueros().subscribe(async data => {
        this.peluqueros = data.data;
        await loading.dismiss();
        console.log(this.peluqueros);
      }, err => {
        console.log(err);
        loading.dismiss();
      });
    });
  }

  async getDiaspeluquero(id) {
    const loading = await this.loading.create({
      message: 'Loading...',
    });
    await loading.present().then(() => {
      this.service.getDiaspeluqero(id).subscribe(async data => {
        this.dias = data.data;
        await loading.dismiss();
        console.log(this.dias);
      }, err => {
        console.log(err);
        loading.dismiss();
      });
    });
  }

  async getHorarios(dia, id) {
    const loading = await this.loading.create({
      message: 'Loading...',
    });
    await loading.present().then(() => {
      this.service.getHorarios(dia, id).subscribe(async data => {
        this.horarios = data.data;
        await loading.dismiss();
        console.log(this.horarios);
      }, err => {
        console.log(err);
        loading.dismiss();
      });
    });
  }

  getPrecios(id) {
  this.service.getPrecio(id).subscribe(async data => {
      this.precio = data.data;
      console.log(this.precio);
    }, err => {
      console.log(err);
    });
  }

  sumaTotal(){
    this.service.getTotal().then(result => {
      this.total = result;
    });
  }

  async cita() {
    const loading = await this.loading.create({
      message: 'Enviando....',
    });
    if (this.form.valid === true ) {
      await loading.present().then(() => {
        this.service.addCita(this.form.value.tercero, this.form.value.segundo,
          this.form.value.primero, this.user, this.total).subscribe(
            async data => {
              this.sms('Se creo la cita correctamente');
              this.service.delete();
              this.nav.navigateRoot('/tabs/tab1');
              await loading.dismiss();
            }, err => {
              console.log(err);
              loading.dismiss();
            });
      });
    } else {
      this.sms('Debe seleccionar todos los campos');
    }
  }

  async sms(m) {
    const toast = await this.toast.create({
      message: m,
      duration: 5000,
    });
    toast.present();
  }

}
