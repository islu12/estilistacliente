import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../services/service.service';
import { LoadingController, ModalController } from '@ionic/angular';
import { ListProductsPage } from '../list-products/list-products.page';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  categorias: any;

  constructor(public service: ServiceService,
              private loading: LoadingController,
              private modal: ModalController
  ) { }

  ngOnInit() {
    this.getCategorias();
  }

  async getCategorias() {
    const loading = await this.loading.create({
      message: 'Loading...',
    });
    await loading.present().then(() => {
      this.service.getCategorias().subscribe(async data => {
        this.categorias = data.data;
        await loading.dismiss();
        console.log(this.categorias);
      }, err => {
        console.log(err);
        loading.dismiss();
      }
      );
    });
  }

  async openModal(item) {
    const modal = await this.modal.create({
      component: ListProductsPage,
      componentProps: {
        'id': item.id,
      }
    });
    return await modal.present();
  }
}
