import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Facebook } from '@ionic-native/facebook/ngx';
import { AlertController, LoadingController, NavController, ToastController } from '@ionic/angular';
import { ServiceService } from '../services/service.service';
import { GooglePlus } from '@ionic-native/google-plus/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  data: any;
  user = { id: '', name: '', email: '', picture: { data: { url: '' } } };
  log = false;
  crear = false;
  datos: any;
  userInfo: any;

  constructor(public loading: LoadingController,
    public formBuilder: FormBuilder,
    private service: ServiceService,
    public nav: NavController,
    public toastCtrl: ToastController,
    private fb: Facebook,
    public router: Router,
    public alert: AlertController,
    public gp: GooglePlus
  ) { }

  ngOnInit() {
  }

  async error(sms) {
    const toast = await this.toastCtrl.create({
      message: sms,
      duration: 5000,
      cssClass: 'my-custom-class-alert'
    });
    toast.present();
  }

  async success(nombre) {
    const toast = await this.toastCtrl.create({
      message: 'Bienvenido ' + nombre,
      duration: 3000,
      cssClass: 'my-custom-class-success'
    });
    toast.present();
  }

  async loginFacebook() {
    const load = await this.loading.create({
      message: 'Iniciando....',
    });
    this.fb.login(['public_profile', 'email'])
      .then(async res => {
        if (res.status === 'connected') {
          this.log = true;
          this.getUser(res.authResponse.userID);
          this.sms();
          load.dismiss();
        } else {
          this.log = false;
        }
      })
      .catch(e => console.log('Error logging into Facebook', e));
  } 

  async getUser(userid: any) {
    const loading = await this.loading.create({
      message: 'Iniciando Sesión....',
    });
    this.fb.api('/' + userid + '/?fields=id,email,name,picture.width(200).height(200)', ['public_profile'])
      .then(res => {
        console.log(res);
        this.user = res;
        if (this.user.email) {
          if (this.user.picture.data.url === undefined) {
            localStorage.setItem('imgFacebook', '');
          } else {
            localStorage.setItem('imgFacebook', this.user.picture.data.url);
          }
          localStorage.setItem('nombreFacebook', this.user.name);
          this.service.loginClient(this.user.email, 2, this.user.name, this.user.id).subscribe(
            async data => {
              this.datos = data.data;
              console.log(this.datos);
              localStorage.setItem('id', this.datos.id);
              this.success(this.user.name);
              this.router.navigate(['/tabs/tab1']);
              loading.dismiss();
            }
          );
        } else {
          this.error('Email no válido');
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  async loginGoogle() {
    const loading = await this.loading.create({
      message: 'registrando....',
    });
    this.gp.login({}).then(async res => {
      this.userInfo = res;
      if (this.userInfo.userId) {
        this.googleUser();
        this.sms();
        await loading.dismiss();
      }
    })
      .catch(err => console.log(err));
    await loading.dismiss();
  }

  googleUser() {
    this.service.loginClient(this.userInfo.email, 1, this.userInfo.displayName, this.userInfo.userId)
      .subscribe(async data => {
        this.datos = data.data;
        console.log(this.datos);
        localStorage.setItem('id', this.datos.id);
        if (this.userInfo.imageUrl === undefined) {
          localStorage.setItem('imgGoogle', '');
        } else {
          localStorage.setItem('imgGoogle', this.userInfo.imageUrl);
        }
        localStorage.setItem('nombreGoogle', this.userInfo.displayName);
        this.success(this.userInfo.displayName);
        this.router.navigate(['/tabs/tab1']);
      });
  }

  async sms() {
    const toast = await this.toastCtrl.create({
      message: 'Espere por favor',
      duration: 5000,
      cssClass: 'my-custom-class-success'
    });
    toast.present();
  }
}
