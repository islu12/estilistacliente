import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../services/service.service';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.page.html',
  styleUrls: ['./detalle.page.scss'],
})
export class DetallePage implements OnInit {

  total: number;
  temp: any[] = [];

  constructor(private service: ServiceService,
              public router: Router
  ) { }

  ngOnInit() {
    this.load();
    this.sumaTotal();
  }

  load() {
    this.service.load().then(async result => {
      this.temp = result;
      console.log(this.temp);
    });
  }

  sumaTotal(){
    this.service.getTotal().then(result => {
      this.total = result;
    });
  }

  async cita() {
    this.router.navigate(['/cita']);
  }

  async back() {
    this.router.navigate(['/tabs/tab2']);
  }

  buttonDisabled() {
    if (this.temp.length === 0) { return true ; }
    return false ;
  }

  remove(key) {
    this.service.removeItem(key);
    this.sumaTotal();
    this.load();
  }

}
