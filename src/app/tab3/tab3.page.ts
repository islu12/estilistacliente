import { Component } from '@angular/core';
import { LoadingController, ModalController } from '@ionic/angular';
import { ServiceService } from '../services/service.service';
import { DetalleCitaPage } from '../detalle-cita/detalle-cita.page';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  success: any[] = [];
  google: any;
  facebook: any;
  none: any;
  nombreFacebook: any;
  nombreGoogle: any;
  citas: any;

  constructor(private loading: LoadingController,
    private service: ServiceService,
    private modal: ModalController
  ) {
    this.perfilCliente();
  }

  async perfilCliente() {
    const loading = await this.loading.create({
      message: 'Cargando....',
    });
    await loading.present().then(async () => {
      this.service.getCitas(localStorage.getItem('id')).subscribe(async data => {
        this.citas = data.data;
        await loading.dismiss();
        console.log(this.citas);
      }, err => {
        console.log(err);
        loading.dismiss();
      });
      if (localStorage.getItem('imgGoogle') === '') {
        this.google = '/assets/icon/profile.jpg';
        this.nombreGoogle = localStorage.getItem('nombreGoogle');
      }
      if (localStorage.getItem('imgFacebook') === '') {
        this.facebook = '/assets/icon/profile.jpg';
        this.nombreFacebook = localStorage.getItem('nombreFacebook');
      }
      if (localStorage.getItem('imgFacebook') == null && localStorage.getItem('imgGoogle') == null) {
        this.none = '/assets/icon/profile.jpg';
      } else if (localStorage.getItem('imgFacebook')) {
        this.facebook = localStorage.getItem('imgFacebook');
        this.nombreFacebook = localStorage.getItem('nombreFacebook');
      } else if (localStorage.getItem('imgGoogle')) {
        this.google = localStorage.getItem('imgGoogle');
        this.nombreGoogle = localStorage.getItem('nombreGoogle');
      }
    });
  }

  async openDetail(item) {
    const modal = await this.modal.create({
      component: DetalleCitaPage,
      componentProps: {
        'fecha': item.fecha,
        'total': item.total,
        'estado': item.estado,
        'dia': item.dia,
        'horas': item.horas,
        'precio': item.precio,
        'servicio': item.servicio,
        'peluquero': item.peluquero
      }
    });
    return await modal.present()
  }

}
