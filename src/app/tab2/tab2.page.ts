import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../services/service.service';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {
  data: any;
  precio: any;
  total: number;
  servicios: any[] = [];
  total2: number;
  temp: any[] = [];
  final: any[] = [];


  constructor(private service: ServiceService,
              public loading: LoadingController,
              public router: Router,
              public alert: AlertController,
              public toast: ToastController
  ) { }

  ngOnInit() {
    this.getServicios();
    this.getService();
  }

  async getServicios() {
    const loading = await this.loading.create({
      message: 'Loading...',
    });
    await loading.present().then(() => {
      this.service.getServicios().subscribe(async data => {
        this.data = data.data;
        await loading.dismiss();
        console.log(this.data);
      }, err => {
        console.log(err);
        loading.dismiss();
      });
    });
  }


  getPrecios(id) {
    this.service.getPrecio(id).subscribe(async data => {
      this.precio = data.data;
      console.log(this.precio);
    }, err => {
      console.log(err);
    });
  }

  async add(item) {
    this.final = null;
    this.service.load().then(async result => {
      this.temp = result;
      this.temp.filter(data => {
        if (data.servicio_id === item.id) {
          console.log('encontro el id en el array');
          this.final = data;
        }
      });

      if (this.final) {
        this.rep('El servicio ya fue agregado!!!');
      } else {
        const alert = await this.alert.create({
          header: 'Agregar Servicio',
          message: '<strong>¿Agregar el servicio?</strong>',
          buttons: [
            {
              text: 'Si',
              handler: () => {
                this.service.addItem(item.id, item.precio, item.nombre);
                this.getService();
              }
            },
            {
              text: 'No',
              role: 'cancel',
              cssClass: 'secondary',
              handler: (blah) => {
              }
            }
          ]
        });
        await alert.present();
      }
    });
  }

  async detalle() {
    this.router.navigate(['/detalle']);
  }

  async getService(){
    this.service.load().then(result => {
      this.servicios = result;
      this.total = this.servicios.length;
    });
    this.sumaTotal();
  }

  sumaTotal(){
    this.service.getTotal().then(result => {
      this.total2 = result;
    });
  }

  async rep(mensaje) {
    const toast = await this.toast.create({
      message: mensaje,
      duration: 3000,
    });
    toast.present();
  }

  buttonDisabled() {
    if (this.temp.length === 0) { return true ; }
    return false ;
  }

}
