import { Component, Input, OnInit } from '@angular/core';
import { ModalController, LoadingController } from '@ionic/angular';
import { ServiceService } from '../services/service.service';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.page.html',
  styleUrls: ['./list-products.page.scss'],
})
export class ListProductsPage implements OnInit {

  @Input() id;
  productos: any[] = [];

  constructor(private modal: ModalController,
              public service: ServiceService,
              private loading: LoadingController
  ) {
    this.getProductos();
  }

  ngOnInit() {
    console.log(this.id);
  }

  dismiss() {
    this.modal.dismiss({
      dismissed: true
    });
  }

  async getProductos() {
    const loading = await this.loading.create({
      message: 'Loading...',
    });
    await loading.present().then(() => {
      this.service.getProductos(this.id).subscribe(async data => {
        this.productos = data.data;
        await loading.dismiss();
        console.log(this.productos);
      }, err => {
        console.log(err);
        loading.dismiss();
      }
      );
    });
  }

}
