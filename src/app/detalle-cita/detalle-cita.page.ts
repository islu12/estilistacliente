import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-detalle-cita',
  templateUrl: './detalle-cita.page.html',
  styleUrls: ['./detalle-cita.page.scss'],
})
export class DetalleCitaPage implements OnInit {

  @Input() fecha: string;
  @Input() total: string;
  @Input() estado: string;
  @Input() dia: string;
  @Input() horas: string;
  @Input() servicio: string;
  @Input() peluquero: string;

  constructor(private modal: ModalController) { }

  ngOnInit() {
  }

  dismiss() {
    this.modal.dismiss({
    });
  }

}
