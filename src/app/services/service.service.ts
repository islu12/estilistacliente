import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  path = 'https://salonbelleza.rnova.net/api/';
  items: any = [];
  public total = 0;

  httpOptions: { headers: HttpHeaders; };

  constructor(private http: HttpClient) {
  }

  loginClient(email, tipo, name, user): Observable<any> {
    var datoaEnviar = {
      'email': email,
      'tipo_login': tipo,
      'nombre_completo': name,
      'username': user
    };
    return this.http.post(this.path + 'cliente/login', datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        })
      );
  }

  getCategorias(): Observable<any> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };
    return this.http.get(this.path + 'cliente/categorias', this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        })
      );
  }

  getProductos(id): Observable<any> {
    var datoaEnviar = {
      'categoria_id': id,
    };
    return this.http.post(this.path + 'cliente/productos', datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        })
      );
  }

  getServicios(): Observable<any> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };
    return this.http.get(this.path + 'cliente/servicios', this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        })
      );
  }

  getPeluqueros(): Observable<any> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };
    return this.http.get(this.path + 'cliente/peluqueros', this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        })
      );
  }

  getDiaspeluqero(id): Observable<any> {
    var datoaEnviar = {
      'peluquero_id': id,
    };
    return this.http.post(this.path + 'cliente/dias', datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        })
      );
  }

  getHorarios(dia, id): Observable<any> {
    const datoaEnviar = {
      'dia': dia,
      'peluquero_id': id
    };
    return this.http.post(this.path + 'cliente/horarios', datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        })
      );
  }

  addCita(horaid, dia, peluid, id, total): Observable<any> {
    const datoaEnviar = {
      'horario_id': horaid,
      'dia': dia,
      'peluquero_id': peluid,
      'cliente_id': id,
      'total': total,
      'datos': this.items
    };
    console.log(datoaEnviar);
    return this.http.post(this.path + 'cliente/crearcita', datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        })
      );
  }

  getPrecio(id): Observable<any> {
    const datoaEnviar = {
      'servicio_id': id
    };
    return this.http.post(this.path + 'cliente/precio', datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        })
      );
  }

  addItem(id, precio, nombre) {
    this.items.push({
      servicio_id: id,
      precio: precio,
      nombre: nombre
    });
  }

  removeItem(ndx) {
    this.items.splice(ndx, 1);
  }

  load() {
    return Promise.resolve(this.items);
  }

  delete() {
    this.items = [];
  }

  getTotal() {
    console.log(this.items);
    this.total = 0;
    if (this.items != null) {
      for (let item of this.items) {
        this.total += parseInt(item.precio);
        console.log(this.total);
      }
    }
    return Promise.resolve(this.total);
  }

  getCitas(id): Observable<any> {
    const datoaEnviar = {
      'cliente_id': id
    };
    return this.http.post(this.path + 'cliente/citas', datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        })
      );
  }
}
