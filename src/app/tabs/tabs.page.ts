import { Component } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import { Facebook } from '@ionic-native/facebook/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(public nav: NavController,
              public alert: AlertController,
              public facebook: Facebook,
              public google: GooglePlus
  ) { }

  async cerrarS() {
    const alert = await this.alert.create({
      cssClass: 'my-custom-class',
      header: 'Confirmar',
      message: '¿<strong>Cerrar Sesión</strong>?',
      buttons: [
        {
          text: 'Si',
          handler: () => {
            if (localStorage.getItem('imgFacebook')) {
              this.facebook.logout();
            } else if (localStorage.getItem('imgGoogle')) {
              this.google.logout();
            }
            localStorage.clear();
            this.nav.navigateRoot('/login');
          }
        }, {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
        }
      ]
    });
    await alert.present();
  }
}
