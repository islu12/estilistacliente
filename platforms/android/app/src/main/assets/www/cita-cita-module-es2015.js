(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["cita-cita-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/cita/cita.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cita/cita.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\">\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>\n      Reservas\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content [fullscreen]=\"true\">\n  <form [formGroup]=\"form\">\n    <ion-card>\n      <ion-card-content>\n        <ion-item>\n          <ion-label>Seleccionar Peluquero</ion-label>\n          <ion-select formControlName=\"primero\" (ionChange)=\"selectOptionValue2($event)\" interface=\"action-sheet\"\n            cancelText=\"Cancelar\">\n            <ion-select-option *ngFor=\"let item of peluqueros\" [value]=\"item.id\">{{item.username}}\n            </ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-card-content>\n    </ion-card>\n    <ion-card *ngIf=\"show2 == true\">\n      <ion-card-content>\n        <ion-item>\n          <ion-label>Seleccionar el día</ion-label>\n          <ion-select formControlName=\"segundo\" [(ngModel)]=\"dos\" (ionChange)=\"selectOptionValue3($event)\"\n            interface=\"action-sheet\" cancelText=\"Cancelar\">\n            <ion-select-option *ngFor=\"let item of dias\" [value]=\"item.dia\">{{item.dia}}\n            </ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-card-content>\n    </ion-card>\n    <ion-card *ngIf=\"show3 == true\">\n      <ion-card-content>\n        <ion-item>\n          <ion-label>Seleccionar la hora</ion-label>\n          <ion-select formControlName=\"tercero\" [(ngModel)]=\"tres\" interface=\"action-sheet\" cancelText=\"Cancelar\">\n            <ion-select-option *ngFor=\"let item of horarios\" [value]=\"item.id\">{{item.horas}}\n            </ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-card-content>\n    </ion-card>\n  </form>\n</ion-content>\n<ion-footer class=\"ion-no-border\">\n  <ion-card>\n    <ion-card-content>\n      <ion-row>\n        <ion-col size=\"6\">\n          <h3><b>Total:</b></h3>\n          <p class=\"wrap3\">\n            {{ total }} Bs.\n          </p>\n        </ion-col>\n        <ion-col size=\"6\">\n          <ion-button id=\"button-success\" (click)=\"cita()\" expand=\"block\" color=\"primary\" shape=\"round\">\n            Siguiente\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n</ion-footer>");

/***/ }),

/***/ "./src/app/cita/cita-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/cita/cita-routing.module.ts ***!
  \*********************************************/
/*! exports provided: CitaPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CitaPageRoutingModule", function() { return CitaPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _cita_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./cita.page */ "./src/app/cita/cita.page.ts");




const routes = [
    {
        path: '',
        component: _cita_page__WEBPACK_IMPORTED_MODULE_3__["CitaPage"]
    }
];
let CitaPageRoutingModule = class CitaPageRoutingModule {
};
CitaPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CitaPageRoutingModule);



/***/ }),

/***/ "./src/app/cita/cita.module.ts":
/*!*************************************!*\
  !*** ./src/app/cita/cita.module.ts ***!
  \*************************************/
/*! exports provided: CitaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CitaPageModule", function() { return CitaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _cita_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./cita-routing.module */ "./src/app/cita/cita-routing.module.ts");
/* harmony import */ var _cita_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./cita.page */ "./src/app/cita/cita.page.ts");







let CitaPageModule = class CitaPageModule {
};
CitaPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _cita_routing_module__WEBPACK_IMPORTED_MODULE_5__["CitaPageRoutingModule"]
        ],
        declarations: [_cita_page__WEBPACK_IMPORTED_MODULE_6__["CitaPage"]]
    })
], CitaPageModule);



/***/ }),

/***/ "./src/app/cita/cita.page.scss":
/*!*************************************!*\
  !*** ./src/app/cita/cita.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NpdGEvY2l0YS5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/cita/cita.page.ts":
/*!***********************************!*\
  !*** ./src/app/cita/cita.page.ts ***!
  \***********************************/
/*! exports provided: CitaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CitaPage", function() { return CitaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/service.service */ "./src/app/services/service.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");





let CitaPage = class CitaPage {
    constructor(service, loading, formB, toast, nav) {
        this.service = service;
        this.loading = loading;
        this.formB = formB;
        this.toast = toast;
        this.nav = nav;
        this.show = false;
        this.show2 = false;
        this.show3 = false;
        this.user = localStorage.getItem('id');
    }
    ngOnInit() {
        this.form = this.formB.group({
            primero: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            segundo: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            tercero: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]
        });
    }
    ionViewWillEnter() {
        this.getPeluqueros();
        this.sumaTotal();
    }
    selectOptionValue2(event) {
        if (event.detail.value) {
            this.getDiaspeluquero(event.detail.value);
            this.id = event.detail.value;
            this.dos = '';
            this.tres = '';
            this.show2 = true;
        }
    }
    selectOptionValue3(event) {
        if (event.detail.value) {
            this.getHorarios(event.detail.value, this.id);
            this.tres = '';
            this.show3 = true;
        }
    }
    getPeluqueros() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loading.create({
                message: 'Loading...',
            });
            yield loading.present().then(() => {
                this.service.getPeluqueros().subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                    this.peluqueros = data.data;
                    yield loading.dismiss();
                    console.log(this.peluqueros);
                }), err => {
                    console.log(err);
                    loading.dismiss();
                });
            });
        });
    }
    getDiaspeluquero(id) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loading.create({
                message: 'Loading...',
            });
            yield loading.present().then(() => {
                this.service.getDiaspeluqero(id).subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                    this.dias = data.data;
                    yield loading.dismiss();
                    console.log(this.dias);
                }), err => {
                    console.log(err);
                    loading.dismiss();
                });
            });
        });
    }
    getHorarios(dia, id) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loading.create({
                message: 'Loading...',
            });
            yield loading.present().then(() => {
                this.service.getHorarios(dia, id).subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                    this.horarios = data.data;
                    yield loading.dismiss();
                    console.log(this.horarios);
                }), err => {
                    console.log(err);
                    loading.dismiss();
                });
            });
        });
    }
    getPrecios(id) {
        this.service.getPrecio(id).subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.precio = data.data;
            console.log(this.precio);
        }), err => {
            console.log(err);
        });
    }
    sumaTotal() {
        this.service.getTotal().then(result => {
            this.total = result;
        });
    }
    cita() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loading.create({
                message: 'Enviando....',
            });
            if (this.form.valid === true) {
                yield loading.present().then(() => {
                    this.service.addCita(this.form.value.tercero, this.form.value.segundo, this.form.value.primero, this.user, this.total).subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                        this.sms('Se creo la cita correctamente');
                        this.service.delete();
                        this.nav.navigateRoot('/tabs/tab1');
                        yield loading.dismiss();
                    }), err => {
                        console.log(err);
                        loading.dismiss();
                    });
                });
            }
            else {
                this.sms('Debe seleccionar todos los campos');
            }
        });
    }
    sms(m) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toast.create({
                message: m,
                duration: 5000,
            });
            toast.present();
        });
    }
};
CitaPage.ctorParameters = () => [
    { type: _services_service_service__WEBPACK_IMPORTED_MODULE_2__["ServiceService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] }
];
CitaPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-cita',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./cita.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/cita/cita.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./cita.page.scss */ "./src/app/cita/cita.page.scss")).default]
    })
], CitaPage);



/***/ })

}]);
//# sourceMappingURL=cita-cita-module-es2015.js.map