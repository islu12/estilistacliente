(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["detalle-detalle-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/detalle/detalle.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/detalle/detalle.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>Servicios Reservados</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-card *ngIf=\"temp.length == 0\">\n    <ion-card-header>\n      <ion-card-title>No tiene servicios</ion-card-title>\n    </ion-card-header>  \n    <ion-card-content>\n      <ion-button color=\"danger\" (click)=\"back()\">Volver</ion-button>\n    </ion-card-content>\n  </ion-card>\n  <ion-list>\n    <ion-item-sliding *ngFor=\"let item of temp; index as key\">\n      <ion-item>\n        <ion-label>\n          <h5>\n            <Span>\n              {{ item.nombre }}\n            </Span>\n          </h5>\n          <h6 class=\"ion-text-center\">\n            {{ item.precio }} Bs.\n          </h6>\n        </ion-label>\n      </ion-item>\n      <ion-item-options>\n        <ion-item-option (click)=\"remove(key)\" color=\"danger\">\n          <ion-icon class=\"icon\" slot=\"end\" ios=\"close-circle-outline\" md=\"close-circle-outline\"></ion-icon>\n        </ion-item-option>\n      </ion-item-options>\n    </ion-item-sliding>\n  </ion-list>\n</ion-content>\n\n<ion-footer class=\"ion-no-border\">\n  <ion-card>\n    <ion-card-content>\n      <ion-row>\n        <ion-col size=\"6\">\n          <h3><b>Total:</b></h3>\n          <p class=\"wrap3\">\n            {{ total }} Bs.\n          </p>\n        </ion-col>\n        <ion-col size=\"6\">\n          <ion-button disabled={{buttonDisabled()}} id=\"button-success\" (click)=\"cita()\" expand=\"block\" color=\"primary\"\n            shape=\"round\">\n            Siguiente\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n</ion-footer>");

/***/ }),

/***/ "./src/app/detalle/detalle-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/detalle/detalle-routing.module.ts ***!
  \***************************************************/
/*! exports provided: DetallePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallePageRoutingModule", function() { return DetallePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _detalle_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./detalle.page */ "./src/app/detalle/detalle.page.ts");




const routes = [
    {
        path: '',
        component: _detalle_page__WEBPACK_IMPORTED_MODULE_3__["DetallePage"]
    }
];
let DetallePageRoutingModule = class DetallePageRoutingModule {
};
DetallePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DetallePageRoutingModule);



/***/ }),

/***/ "./src/app/detalle/detalle.module.ts":
/*!*******************************************!*\
  !*** ./src/app/detalle/detalle.module.ts ***!
  \*******************************************/
/*! exports provided: DetallePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallePageModule", function() { return DetallePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _detalle_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./detalle-routing.module */ "./src/app/detalle/detalle-routing.module.ts");
/* harmony import */ var _detalle_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detalle.page */ "./src/app/detalle/detalle.page.ts");







let DetallePageModule = class DetallePageModule {
};
DetallePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _detalle_routing_module__WEBPACK_IMPORTED_MODULE_5__["DetallePageRoutingModule"]
        ],
        declarations: [_detalle_page__WEBPACK_IMPORTED_MODULE_6__["DetallePage"]]
    })
], DetallePageModule);



/***/ }),

/***/ "./src/app/detalle/detalle.page.scss":
/*!*******************************************!*\
  !*** ./src/app/detalle/detalle.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".icon {\n  font-size: 30px;\n}\n\nion-card-content md ios {\n  font-size: 50px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGV0YWxsZS9kZXRhbGxlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGVBQUE7QUFDSjs7QUFFQTtFQUNJLGVBQUE7QUFDSiIsImZpbGUiOiJzcmMvYXBwL2RldGFsbGUvZGV0YWxsZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaWNvbiB7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbn1cclxuXHJcbmlvbi1jYXJkLWNvbnRlbnQgbWQgaW9zIHtcclxuICAgIGZvbnQtc2l6ZTogNTBweDtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/detalle/detalle.page.ts":
/*!*****************************************!*\
  !*** ./src/app/detalle/detalle.page.ts ***!
  \*****************************************/
/*! exports provided: DetallePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallePage", function() { return DetallePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/service.service */ "./src/app/services/service.service.ts");




let DetallePage = class DetallePage {
    constructor(service, router) {
        this.service = service;
        this.router = router;
        this.temp = [];
    }
    ngOnInit() {
        this.load();
        this.sumaTotal();
    }
    load() {
        this.service.load().then((result) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.temp = result;
            console.log(this.temp);
        }));
    }
    sumaTotal() {
        this.service.getTotal().then(result => {
            this.total = result;
        });
    }
    cita() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.router.navigate(['/cita']);
        });
    }
    back() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.router.navigate(['/tabs/tab2']);
        });
    }
    buttonDisabled() {
        if (this.temp.length === 0) {
            return true;
        }
        return false;
    }
    remove(key) {
        this.service.removeItem(key);
        this.sumaTotal();
        this.load();
    }
};
DetallePage.ctorParameters = () => [
    { type: _services_service_service__WEBPACK_IMPORTED_MODULE_3__["ServiceService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
DetallePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-detalle',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./detalle.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/detalle/detalle.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./detalle.page.scss */ "./src/app/detalle/detalle.page.scss")).default]
    })
], DetallePage);



/***/ })

}]);
//# sourceMappingURL=detalle-detalle-module-es2015.js.map