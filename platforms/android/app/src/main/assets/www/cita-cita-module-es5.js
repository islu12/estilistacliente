(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["cita-cita-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/cita/cita.page.html":
    /*!***************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cita/cita.page.html ***!
      \***************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppCitaCitaPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header [translucent]=\"true\">\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>\n      Reservas\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content [fullscreen]=\"true\">\n  <form [formGroup]=\"form\">\n    <ion-card>\n      <ion-card-content>\n        <ion-item>\n          <ion-label>Seleccionar Peluquero</ion-label>\n          <ion-select formControlName=\"primero\" (ionChange)=\"selectOptionValue2($event)\" interface=\"action-sheet\"\n            cancelText=\"Cancelar\">\n            <ion-select-option *ngFor=\"let item of peluqueros\" [value]=\"item.id\">{{item.username}}\n            </ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-card-content>\n    </ion-card>\n    <ion-card *ngIf=\"show2 == true\">\n      <ion-card-content>\n        <ion-item>\n          <ion-label>Seleccionar el día</ion-label>\n          <ion-select formControlName=\"segundo\" [(ngModel)]=\"dos\" (ionChange)=\"selectOptionValue3($event)\"\n            interface=\"action-sheet\" cancelText=\"Cancelar\">\n            <ion-select-option *ngFor=\"let item of dias\" [value]=\"item.dia\">{{item.dia}}\n            </ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-card-content>\n    </ion-card>\n    <ion-card *ngIf=\"show3 == true\">\n      <ion-card-content>\n        <ion-item>\n          <ion-label>Seleccionar la hora</ion-label>\n          <ion-select formControlName=\"tercero\" [(ngModel)]=\"tres\" interface=\"action-sheet\" cancelText=\"Cancelar\">\n            <ion-select-option *ngFor=\"let item of horarios\" [value]=\"item.id\">{{item.horas}}\n            </ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-card-content>\n    </ion-card>\n  </form>\n</ion-content>\n<ion-footer class=\"ion-no-border\">\n  <ion-card>\n    <ion-card-content>\n      <ion-row>\n        <ion-col size=\"6\">\n          <h3><b>Total:</b></h3>\n          <p class=\"wrap3\">\n            {{ total }} Bs.\n          </p>\n        </ion-col>\n        <ion-col size=\"6\">\n          <ion-button id=\"button-success\" (click)=\"cita()\" expand=\"block\" color=\"primary\" shape=\"round\">\n            Siguiente\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n</ion-footer>";
      /***/
    },

    /***/
    "./src/app/cita/cita-routing.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/cita/cita-routing.module.ts ***!
      \*********************************************/

    /*! exports provided: CitaPageRoutingModule */

    /***/
    function srcAppCitaCitaRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CitaPageRoutingModule", function () {
        return CitaPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _cita_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./cita.page */
      "./src/app/cita/cita.page.ts");

      var routes = [{
        path: '',
        component: _cita_page__WEBPACK_IMPORTED_MODULE_3__["CitaPage"]
      }];

      var CitaPageRoutingModule = function CitaPageRoutingModule() {
        _classCallCheck(this, CitaPageRoutingModule);
      };

      CitaPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], CitaPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/cita/cita.module.ts":
    /*!*************************************!*\
      !*** ./src/app/cita/cita.module.ts ***!
      \*************************************/

    /*! exports provided: CitaPageModule */

    /***/
    function srcAppCitaCitaModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CitaPageModule", function () {
        return CitaPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _cita_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./cita-routing.module */
      "./src/app/cita/cita-routing.module.ts");
      /* harmony import */


      var _cita_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./cita.page */
      "./src/app/cita/cita.page.ts");

      var CitaPageModule = function CitaPageModule() {
        _classCallCheck(this, CitaPageModule);
      };

      CitaPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _cita_routing_module__WEBPACK_IMPORTED_MODULE_5__["CitaPageRoutingModule"]],
        declarations: [_cita_page__WEBPACK_IMPORTED_MODULE_6__["CitaPage"]]
      })], CitaPageModule);
      /***/
    },

    /***/
    "./src/app/cita/cita.page.scss":
    /*!*************************************!*\
      !*** ./src/app/cita/cita.page.scss ***!
      \*************************************/

    /*! exports provided: default */

    /***/
    function srcAppCitaCitaPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NpdGEvY2l0YS5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/cita/cita.page.ts":
    /*!***********************************!*\
      !*** ./src/app/cita/cita.page.ts ***!
      \***********************************/

    /*! exports provided: CitaPage */

    /***/
    function srcAppCitaCitaPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CitaPage", function () {
        return CitaPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _services_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../services/service.service */
      "./src/app/services/service.service.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");

      var CitaPage = /*#__PURE__*/function () {
        function CitaPage(service, loading, formB, toast, nav) {
          _classCallCheck(this, CitaPage);

          this.service = service;
          this.loading = loading;
          this.formB = formB;
          this.toast = toast;
          this.nav = nav;
          this.show = false;
          this.show2 = false;
          this.show3 = false;
          this.user = localStorage.getItem('id');
        }

        _createClass(CitaPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.form = this.formB.group({
              primero: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
              segundo: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
              tercero: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]
            });
          }
        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            this.getPeluqueros();
            this.sumaTotal();
          }
        }, {
          key: "selectOptionValue2",
          value: function selectOptionValue2(event) {
            if (event.detail.value) {
              this.getDiaspeluquero(event.detail.value);
              this.id = event.detail.value;
              this.dos = '';
              this.tres = '';
              this.show2 = true;
            }
          }
        }, {
          key: "selectOptionValue3",
          value: function selectOptionValue3(event) {
            if (event.detail.value) {
              this.getHorarios(event.detail.value, this.id);
              this.tres = '';
              this.show3 = true;
            }
          }
        }, {
          key: "getPeluqueros",
          value: function getPeluqueros() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this = this;

              var loading;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.loading.create({
                        message: 'Loading...'
                      });

                    case 2:
                      loading = _context2.sent;
                      _context2.next = 5;
                      return loading.present().then(function () {
                        _this.service.getPeluqueros().subscribe(function (data) {
                          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                            return regeneratorRuntime.wrap(function _callee$(_context) {
                              while (1) {
                                switch (_context.prev = _context.next) {
                                  case 0:
                                    this.peluqueros = data.data;
                                    _context.next = 3;
                                    return loading.dismiss();

                                  case 3:
                                    console.log(this.peluqueros);

                                  case 4:
                                  case "end":
                                    return _context.stop();
                                }
                              }
                            }, _callee, this);
                          }));
                        }, function (err) {
                          console.log(err);
                          loading.dismiss();
                        });
                      });

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "getDiaspeluquero",
          value: function getDiaspeluquero(id) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var _this2 = this;

              var loading;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      _context4.next = 2;
                      return this.loading.create({
                        message: 'Loading...'
                      });

                    case 2:
                      loading = _context4.sent;
                      _context4.next = 5;
                      return loading.present().then(function () {
                        _this2.service.getDiaspeluqero(id).subscribe(function (data) {
                          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                            return regeneratorRuntime.wrap(function _callee3$(_context3) {
                              while (1) {
                                switch (_context3.prev = _context3.next) {
                                  case 0:
                                    this.dias = data.data;
                                    _context3.next = 3;
                                    return loading.dismiss();

                                  case 3:
                                    console.log(this.dias);

                                  case 4:
                                  case "end":
                                    return _context3.stop();
                                }
                              }
                            }, _callee3, this);
                          }));
                        }, function (err) {
                          console.log(err);
                          loading.dismiss();
                        });
                      });

                    case 5:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "getHorarios",
          value: function getHorarios(dia, id) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
              var _this3 = this;

              var loading;
              return regeneratorRuntime.wrap(function _callee6$(_context6) {
                while (1) {
                  switch (_context6.prev = _context6.next) {
                    case 0:
                      _context6.next = 2;
                      return this.loading.create({
                        message: 'Loading...'
                      });

                    case 2:
                      loading = _context6.sent;
                      _context6.next = 5;
                      return loading.present().then(function () {
                        _this3.service.getHorarios(dia, id).subscribe(function (data) {
                          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
                            return regeneratorRuntime.wrap(function _callee5$(_context5) {
                              while (1) {
                                switch (_context5.prev = _context5.next) {
                                  case 0:
                                    this.horarios = data.data;
                                    _context5.next = 3;
                                    return loading.dismiss();

                                  case 3:
                                    console.log(this.horarios);

                                  case 4:
                                  case "end":
                                    return _context5.stop();
                                }
                              }
                            }, _callee5, this);
                          }));
                        }, function (err) {
                          console.log(err);
                          loading.dismiss();
                        });
                      });

                    case 5:
                    case "end":
                      return _context6.stop();
                  }
                }
              }, _callee6, this);
            }));
          }
        }, {
          key: "getPrecios",
          value: function getPrecios(id) {
            var _this4 = this;

            this.service.getPrecio(id).subscribe(function (data) {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this4, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
                return regeneratorRuntime.wrap(function _callee7$(_context7) {
                  while (1) {
                    switch (_context7.prev = _context7.next) {
                      case 0:
                        this.precio = data.data;
                        console.log(this.precio);

                      case 2:
                      case "end":
                        return _context7.stop();
                    }
                  }
                }, _callee7, this);
              }));
            }, function (err) {
              console.log(err);
            });
          }
        }, {
          key: "sumaTotal",
          value: function sumaTotal() {
            var _this5 = this;

            this.service.getTotal().then(function (result) {
              _this5.total = result;
            });
          }
        }, {
          key: "cita",
          value: function cita() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
              var _this6 = this;

              var loading;
              return regeneratorRuntime.wrap(function _callee9$(_context9) {
                while (1) {
                  switch (_context9.prev = _context9.next) {
                    case 0:
                      _context9.next = 2;
                      return this.loading.create({
                        message: 'Enviando....'
                      });

                    case 2:
                      loading = _context9.sent;

                      if (!(this.form.valid === true)) {
                        _context9.next = 8;
                        break;
                      }

                      _context9.next = 6;
                      return loading.present().then(function () {
                        _this6.service.addCita(_this6.form.value.tercero, _this6.form.value.segundo, _this6.form.value.primero, _this6.user, _this6.total).subscribe(function (data) {
                          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this6, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
                            return regeneratorRuntime.wrap(function _callee8$(_context8) {
                              while (1) {
                                switch (_context8.prev = _context8.next) {
                                  case 0:
                                    this.sms('Se creo la cita correctamente');
                                    this.service["delete"]();
                                    this.nav.navigateRoot('/tabs/tab1');
                                    _context8.next = 5;
                                    return loading.dismiss();

                                  case 5:
                                  case "end":
                                    return _context8.stop();
                                }
                              }
                            }, _callee8, this);
                          }));
                        }, function (err) {
                          console.log(err);
                          loading.dismiss();
                        });
                      });

                    case 6:
                      _context9.next = 9;
                      break;

                    case 8:
                      this.sms('Debe seleccionar todos los campos');

                    case 9:
                    case "end":
                      return _context9.stop();
                  }
                }
              }, _callee9, this);
            }));
          }
        }, {
          key: "sms",
          value: function sms(m) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee10() {
              var toast;
              return regeneratorRuntime.wrap(function _callee10$(_context10) {
                while (1) {
                  switch (_context10.prev = _context10.next) {
                    case 0:
                      _context10.next = 2;
                      return this.toast.create({
                        message: m,
                        duration: 5000
                      });

                    case 2:
                      toast = _context10.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context10.stop();
                  }
                }
              }, _callee10, this);
            }));
          }
        }]);

        return CitaPage;
      }();

      CitaPage.ctorParameters = function () {
        return [{
          type: _services_service_service__WEBPACK_IMPORTED_MODULE_2__["ServiceService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
        }];
      };

      CitaPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-cita',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./cita.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/cita/cita.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./cita.page.scss */
        "./src/app/cita/cita.page.scss"))["default"]]
      })], CitaPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=cita-cita-module-es5.js.map